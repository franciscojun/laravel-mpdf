### mPDF Wrapper for Laravel 5

## Installation
Begin by installing this package through Composer. Edit your project's `composer.json` file to require `franciscojun/laravel-mpdf`.

```json
{
  "require": {
       "franciscojun/laravel-mpdf" : "dev-master"
    }
}
```

Next, update Composer from the Terminal:
```bash
$ composer update
```

Once this operation completes, the final step is to add the service provider. Open `app/config/app.php`, and add a new item to the providers array.
```php
'Franciscojun\Mpdf\ServiceProvider',
```

You can also register facade.


```php
'PDF' => 'Franciscojun\Mpdf\Facades\Pdf',
```


### License

This mPDF Wrapper for Laravel5 is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
